<?php
try {
    // Vérifier si une session est déjà démarrée
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    // Le reste de votre code ici...

} catch (Exception $e) {
    //echo "Une erreur s'est produite : " . $e->getMessage();
}
?>
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $titre;?></title>
    <link href ="accueil2.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  </head>
  <body>