<?php
    include "header.inc.php";
    include "menu.inc.php";
 $email = htmlentities($_POST['email']);
 $pass = htmlentities($_POST['password']);

 $options = ['cost' => 12,];

 mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
 require_once("parametre_con.php");
 $conn = new mysqli($host,$login,$password,$db_name);
    if($conn->connect_error){
        die("La connexion à la base de données a échoué: ".$conn->connect_error);
    }
 try{
    $statement = $conn->prepare("SELECT mot_de_passe, type_user, Nom_user_U AS nom,prenom_U AS prenom FROM
    User_U WHERE email like ?");
    $statement->bind_param("s",$email);
    $statement->execute();
    $result = $statement->get_result();
    //echo "check the email";
    if($result->num_rows>0){
        $res = $result->fetch_assoc();
        if(password_verify($pass,$res["mot_de_passe"])){
            $full_name = $res["prenom"]." ".$res["nom"];
            $_SESSION["Message"] = "<h1>BIENVENUE ".$full_name." !!</h1><br>
            <br>
            Vous etes connectée en tant qu";
            if($res["type_user"] == 1){
                $_SESSION["Message"] =$_SESSION["Message"]."e membre du site ";
            }
            if($res["type_user"] == 2){
                $_SESSION["Message"] =$_SESSION["Message"]."'administrateur du site ";
            }
            echo "<meta http-equiv='refresh' content='0;url=conected.php>";
        }
        else{
            $_SESSION["Message"] = "Mot de passe incorrect";
            require_once("A_propos.php");
            goto_page("erreur_inscription.php");
        }
    }        
    else{
        $_SESSION["Message"] = "Cet email n'existe pas";
        require_once("A_propos.php");
        goto_page("erreur_inscription.php");
    }

 }
 catch(mysql_sql_exception $e){ 
    $_SESSION["Message"]= $e->getMessage();
    require_once("A_propos.php");
    goto_page("erreur_inscription.php"); 
 }
 require_once("A_propos.php");
       goto_page("erreur_inscription.php");
?>


<?php
    include "footer.inc.php";
?>